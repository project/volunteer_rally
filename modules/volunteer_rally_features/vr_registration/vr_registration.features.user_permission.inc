<?php
/**
 * @file
 * vr_registration.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function vr_registration_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer own volunteer registration'.
  $permissions['administer own volunteer registration'] = array(
    'name' => 'administer own volunteer registration',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'administer registration'.
  $permissions['administer registration'] = array(
    'name' => 'administer registration',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'administer registration states'.
  $permissions['administer registration states'] = array(
    'name' => 'administer registration states',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'administer registration types'.
  $permissions['administer registration types'] = array(
    'name' => 'administer registration types',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'administer volunteer registration'.
  $permissions['administer volunteer registration'] = array(
    'name' => 'administer volunteer registration',
    'roles' => array(),
    'module' => 'registration',
  );

  // Exported permission: 'create volunteer registration'.
  $permissions['create volunteer registration'] = array(
    'name' => 'create volunteer registration',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'create volunteer registration other anonymous'.
  $permissions['create volunteer registration other anonymous'] = array(
    'name' => 'create volunteer registration other anonymous',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'create volunteer registration other users'.
  $permissions['create volunteer registration other users'] = array(
    'name' => 'create volunteer registration other users',
    'roles' => array(),
    'module' => 'registration',
  );

  // Exported permission: 'delete any volunteer registration'.
  $permissions['delete any volunteer registration'] = array(
    'name' => 'delete any volunteer registration',
    'roles' => array(),
    'module' => 'registration',
  );

  // Exported permission: 'delete own volunteer registration'.
  $permissions['delete own volunteer registration'] = array(
    'name' => 'delete own volunteer registration',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'edit volunteer registration state'.
  $permissions['edit volunteer registration state'] = array(
    'name' => 'edit volunteer registration state',
    'roles' => array(),
    'module' => 'registration',
  );

  // Exported permission: 'update any volunteer registration'.
  $permissions['update any volunteer registration'] = array(
    'name' => 'update any volunteer registration',
    'roles' => array(),
    'module' => 'registration',
  );

  // Exported permission: 'update own volunteer registration'.
  $permissions['update own volunteer registration'] = array(
    'name' => 'update own volunteer registration',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'view own volunteer registration'.
  $permissions['view own volunteer registration'] = array(
    'name' => 'view own volunteer registration',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'view volunteer registration'.
  $permissions['view volunteer registration'] = array(
    'name' => 'view volunteer registration',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'registration',
  );

  return $permissions;
}
