<?php
/**
 * @file
 * vr_registration.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function vr_registration_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_register'
  $field_bases['field_register'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_register',
    'foreign keys' => array(
      'registration_type' => array(
        'columns' => array(
          'registration_type' => 'name',
        ),
        'table' => 'registration_type',
      ),
    ),
    'indexes' => array(
      'registration_type' => array(
        0 => 'registration_type',
      ),
    ),
    'locked' => 0,
    'module' => 'registration',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'registration',
  );

  return $field_bases;
}
