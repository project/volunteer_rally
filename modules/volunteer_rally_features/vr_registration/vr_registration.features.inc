<?php
/**
 * @file
 * vr_registration.features.inc
 */

/**
 * Implements hook_default_registration_state().
 */
function vr_registration_default_registration_state() {
  $items = array();
  $items['complete'] = entity_import('registration_state', '{
    "name" : "complete",
    "label" : "Complete",
    "description" : "Registration has been completed.",
    "default_state" : "1",
    "active" : "1",
    "show_on_form" : "0",
    "weight" : "1"
  }');
  return $items;
}

/**
 * Implements hook_default_registration_type().
 */
function vr_registration_default_registration_type() {
  $items = array();
  $items['volunteer'] = entity_import('registration_type', '{
    "name" : "volunteer",
    "label" : "Volunteer",
    "locked" : "0",
    "weight" : "0",
    "data" : null
  }');
  return $items;
}
