<?php
/**
 * @file
 * vr_registration.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function vr_registration_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-shift-field_register'
  $field_instances['node-shift-field_register'] = array(
    'bundle' => 'shift',
    'default_value' => array(
      0 => array(
        'registration_type' => 'volunteer',
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'registration',
        'settings' => array(
          'label' => ' Register Here!',
        ),
        'type' => 'registration_link',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_register',
    'label' => 'Register',
    'required' => 0,
    'settings' => array(
      'default_registration_settings' => array(
        'capacity' => 0,
        'reminder' => array(
          'reminder_settings' => array(
            'reminder_date' => NULL,
            'reminder_template' => '',
          ),
          'send_reminder' => 0,
        ),
        'scheduling' => array(
          'close' => NULL,
          'open' => NULL,
        ),
        'settings' => array(
          'confirmation' => 'Registration has been saved.',
          'from_address' => 'austin@opensourcery.com',
          'maximum_spaces' => 1,
          'multiple_registrations' => 0,
          'registration_entity_access_roles' => array(
            2 => 2,
            3 => 3,
            4 => 4,
          ),
          'registration_waitlist_capacity' => 20,
          'registration_waitlist_enable' => 1,
        ),
        'status' => 1,
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'registration',
      'settings' => array(),
      'type' => 'registration_select',
      'weight' => 3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Register');

  return $field_instances;
}
