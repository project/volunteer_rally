<?php
/**
 * @file
 * vr_priority.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function vr_priority_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function vr_priority_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implements hook_node_info().
 */
function vr_priority_node_info() {
  $items = array(
    'priority_code' => array(
      'name' => t('Priority code'),
      'base' => 'node_content',
      'description' => t('A code that can be associated with a shift in order to restrict access to the shift for those having the specific URL.'),
      'has_title' => '1',
      'title_label' => t('Code'),
      'help' => '',
    ),
  );
  return $items;
}
