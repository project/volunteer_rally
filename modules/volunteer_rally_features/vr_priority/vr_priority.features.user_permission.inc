<?php
/**
 * @file
 * vr_priority.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function vr_priority_user_default_permissions() {
  $permissions = array();

  // Exported permission: create priority_code content
  $permissions['create priority_code content'] = array(
    'name' => 'create priority_code content',
    'roles' => array(
      0 => 'volunteer coordinator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any priority_code content
  $permissions['delete any priority_code content'] = array(
    'name' => 'delete any priority_code content',
    'roles' => array(
      0 => 'volunteer coordinator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own priority_code content
  $permissions['delete own priority_code content'] = array(
    'name' => 'delete own priority_code content',
    'roles' => array(
      0 => 'volunteer coordinator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any priority_code content
  $permissions['edit any priority_code content'] = array(
    'name' => 'edit any priority_code content',
    'roles' => array(
      0 => 'volunteer coordinator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own priority_code content
  $permissions['edit own priority_code content'] = array(
    'name' => 'edit own priority_code content',
    'roles' => array(
      0 => 'volunteer coordinator',
    ),
    'module' => 'node',
  );

  // Exported permission: manage vr priority codes
  $permissions['manage vr priority codes'] = array(
    'name' => 'manage vr priority codes',
    'roles' => array(
      0 => 'volunteer coordinator',
    ),
    'module' => 'vr_priority',
  );

  return $permissions;
}
