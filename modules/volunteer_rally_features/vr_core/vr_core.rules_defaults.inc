<?php
/**
 * @file
 * vr_core.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function vr_core_default_rules_configuration() {
  $items = array();
  $items['rules_clear_shifts_view_cache'] = entity_import('rules_config', '{ "rules_clear_shifts_view_cache" : {
      "LABEL" : "Clear shifts view cache",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Volunteer Rally" ],
      "REQUIRES" : [ "rules", "cache_actions" ],
      "ON" : { "node_insert" : [], "node_delete" : [], "node_update" : [] },
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "shift" : "shift" } } } }
      ],
      "DO" : [
        { "cache_actions_action_clear_views_cache" : { "view" : { "value" : { "shifts" : "shifts" } } } }
      ]
    }
  }');
  return $items;
}
