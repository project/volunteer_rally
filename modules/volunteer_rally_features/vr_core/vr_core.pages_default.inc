<?php
/**
 * @file
 * vr_core.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function vr_core_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'vr_site_template_panel_context';
  $handler->task = 'site_template';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -30;
  $handler->conf = array(
    'title' => 'Volunteer Rally Panels Everywhere site template',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'atpe_site_template';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'sidebar_first' => NULL,
      'sidebar_second' => NULL,
      'content' => NULL,
      'highlighted' => NULL,
      'content_aside' => NULL,
      'secondary_content' => NULL,
      'tertiary_content' => NULL,
      'footer' => NULL,
      'leaderboard' => NULL,
      'header' => NULL,
      'menu_bar' => NULL,
      'help' => NULL,
      'three_25_50_25_top' => NULL,
      'three_25_50_25_first' => NULL,
      'three_25_50_25_second' => NULL,
      'three_25_50_25_third' => NULL,
      'three_25_50_25_bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '1fc40d41-f2a5-49ab-bb04-c879dc4b993c';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-f0daa54b-cee7-426d-84bb-0f198d19386d';
    $pane->panel = 'content';
    $pane->type = 'pane_messages';
    $pane->subtype = 'pane_messages';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'f0daa54b-cee7-426d-84bb-0f198d19386d';
    $display->content['new-f0daa54b-cee7-426d-84bb-0f198d19386d'] = $pane;
    $display->panels['content'][0] = 'new-f0daa54b-cee7-426d-84bb-0f198d19386d';
    $pane = new stdClass();
    $pane->pid = 'new-f887deef-cb88-4d66-a06c-beb5ed9eab47';
    $pane->panel = 'content';
    $pane->type = 'page_content';
    $pane->subtype = 'page_content';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_page_content_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'f887deef-cb88-4d66-a06c-beb5ed9eab47';
    $display->content['new-f887deef-cb88-4d66-a06c-beb5ed9eab47'] = $pane;
    $display->panels['content'][1] = 'new-f887deef-cb88-4d66-a06c-beb5ed9eab47';
    $pane = new stdClass();
    $pane->pid = 'new-9b523748-c6a5-4dfd-95f4-a478c2c50e1f';
    $pane->panel = 'footer';
    $pane->type = 'block';
    $pane->subtype = 'boxes-vr_footer';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '9b523748-c6a5-4dfd-95f4-a478c2c50e1f';
    $display->content['new-9b523748-c6a5-4dfd-95f4-a478c2c50e1f'] = $pane;
    $display->panels['footer'][0] = 'new-9b523748-c6a5-4dfd-95f4-a478c2c50e1f';
    $pane = new stdClass();
    $pane->pid = 'new-564b5432-8fac-4d3b-8e97-b7872dc345c1';
    $pane->panel = 'header';
    $pane->type = 'pane_header';
    $pane->subtype = 'pane_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '564b5432-8fac-4d3b-8e97-b7872dc345c1';
    $display->content['new-564b5432-8fac-4d3b-8e97-b7872dc345c1'] = $pane;
    $display->panels['header'][0] = 'new-564b5432-8fac-4d3b-8e97-b7872dc345c1';
    $pane = new stdClass();
    $pane->pid = 'new-f8b005f4-4c5b-41e3-b5f0-1e51d33e7526';
    $pane->panel = 'leaderboard';
    $pane->type = 'block';
    $pane->subtype = 'system-user-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'f8b005f4-4c5b-41e3-b5f0-1e51d33e7526';
    $display->content['new-f8b005f4-4c5b-41e3-b5f0-1e51d33e7526'] = $pane;
    $display->panels['leaderboard'][0] = 'new-f8b005f4-4c5b-41e3-b5f0-1e51d33e7526';
    $pane = new stdClass();
    $pane->pid = 'new-ec194d7d-e5d7-4d33-aa3f-cf3640e4d5d6';
    $pane->panel = 'menu_bar';
    $pane->type = 'pane_navigation';
    $pane->subtype = 'pane_navigation';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'ec194d7d-e5d7-4d33-aa3f-cf3640e4d5d6';
    $display->content['new-ec194d7d-e5d7-4d33-aa3f-cf3640e4d5d6'] = $pane;
    $display->panels['menu_bar'][0] = 'new-ec194d7d-e5d7-4d33-aa3f-cf3640e4d5d6';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-564b5432-8fac-4d3b-8e97-b7872dc345c1';
  $handler->conf['display'] = $display;
  $export['vr_site_template_panel_context'] = $handler;

  return $export;
}

/**
 * Implements hook_default_page_manager_pages().
 */
function vr_core_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'vr_front';
  $page->task = 'page';
  $page->admin_title = 'Volunteer Rally home page';
  $page->admin_description = '';
  $page->path = 'welcome';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Calendar',
    'name' => 'main-menu',
    'weight' => '-42',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_vr_front_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'vr_front';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '0b3c973e-44c4-4ac6-9c5e-3190ad9b9c9f';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-fb9b7da0-1b9a-4d52-a279-0066130cf285';
    $pane->panel = 'middle';
    $pane->type = 'block';
    $pane->subtype = 'boxes-vr_welcome';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'fb9b7da0-1b9a-4d52-a279-0066130cf285';
    $display->content['new-fb9b7da0-1b9a-4d52-a279-0066130cf285'] = $pane;
    $display->panels['middle'][0] = 'new-fb9b7da0-1b9a-4d52-a279-0066130cf285';
    $pane = new stdClass();
    $pane->pid = 'new-9d76cfc3-b345-45e9-a5fb-26c504c99631';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'shifts-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '9d76cfc3-b345-45e9-a5fb-26c504c99631';
    $display->content['new-9d76cfc3-b345-45e9-a5fb-26c504c99631'] = $pane;
    $display->panels['middle'][1] = 'new-9d76cfc3-b345-45e9-a5fb-26c504c99631';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['vr_front'] = $page;

  return $pages;

}
