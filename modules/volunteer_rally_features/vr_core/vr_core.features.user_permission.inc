<?php
/**
 * @file
 * vr_core.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function vr_core_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create shift content'.
  $permissions['create shift content'] = array(
    'name' => 'create shift content',
    'roles' => array(
      'volunteer coordinator' => 'volunteer coordinator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any shift content'.
  $permissions['delete any shift content'] = array(
    'name' => 'delete any shift content',
    'roles' => array(
      'volunteer coordinator' => 'volunteer coordinator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own shift content'.
  $permissions['delete own shift content'] = array(
    'name' => 'delete own shift content',
    'roles' => array(
      'volunteer coordinator' => 'volunteer coordinator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any shift content'.
  $permissions['edit any shift content'] = array(
    'name' => 'edit any shift content',
    'roles' => array(
      'volunteer coordinator' => 'volunteer coordinator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own shift content'.
  $permissions['edit own shift content'] = array(
    'name' => 'edit own shift content',
    'roles' => array(
      'volunteer coordinator' => 'volunteer coordinator',
    ),
    'module' => 'node',
  );

  return $permissions;
}
