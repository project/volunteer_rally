<?php
/**
 * @file
 * vr_core.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function vr_core_user_default_roles() {
  $roles = array();

  // Exported role: volunteer coordinator.
  $roles['volunteer coordinator'] = array(
    'name' => 'volunteer coordinator',
    'weight' => 2,
  );

  return $roles;
}
