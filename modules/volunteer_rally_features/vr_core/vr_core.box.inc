<?php
/**
 * @file
 * vr_core.box.inc
 */

/**
 * Implements hook_default_box().
 */
function vr_core_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'vr_footer';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Default Volunteer Rally footer';
  $box->options = array(
    'body' => array(
      'value' => 'Volunteer Rally&trade; &mdash; built by <a href="http://www.opensourcery.com">OpenSourcery</a>',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['vr_footer'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'vr_welcome';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Home page block.';
  $box->options = array(
    'body' => array(
      'value' => 'Welcome to your <strong>Volunteer Rally</strong> installation. Edit this text to add your own welcome message or remove this one.',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['vr_welcome'] = $box;

  return $export;
}
