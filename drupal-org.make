; Make file for Volunteer Rally

core = 7.x
api = 2

projects[drupal][patch][] = "https://www.drupal.org/files/1093420-22.patch"

includes[theme] = "theme.make"


; Set contrib directory.
defaults[projects][subdir] = "contrib"

projects[achievements][version] = "1.5"

projects[addressfield][version] = "1.0-beta5"

projects[auto_nodetitle][version] = "1.0"

projects[borealis][version] = "2.2"

projects[boxes][version] = "1.1"

projects[cache_actions][version] = "2.0-alpha5"

projects[context][version] = "3.2"

projects[ctools][version] = "1.4"

projects[date][version] = "2.7"

projects[elements][version] = "1.4"

projects[entity][version] = "1.5"

projects[entityreference][version] = "1.1"

projects[entityreference_prepopulate][version] = "1.5"

projects[features][version] = "2.0"

projects[fences][version] = "1.0"
projects[fences][download][revision] = "9c6d7d6"

projects[fullcalendar][version] = "2.0"

projects[html5_tools][version] = "1.2"

projects[jquery_update][version] = "2.4"

projects[libraries][version] = "2.2"

projects[node_recur][version] = "1.0-beta4"

projects[og][version] = "2.7"

projects[override_node_options][version] = "1.12"

projects[pathauto][version] = "1.2"

projects[panels][version] = "3.4"

projects[purl][version] = "1.0-beta1"

projects[inline_entity_form][version] = "1.5"

projects[registration][version] = "1.3"

projects[role_delegation][version] = "1.1"

projects[rules][version] = "2.6"

projects[strongarm][version] = "2.0"

projects[token][version] = "1.5"

projects[views][version] = "3.7"
projects[views][patch][] = "https://drupal.org/files/views-1511396-110-array_diff_recursive.patch"

projects[views_bulk_operations][version] = "3.2"

projects[wysiwyg][version] = "2.2"

; Profiler
libraries[profiler][download][type] = "get"
libraries[profiler][download][url] = "http://ftp.drupal.org/files/projects/profiler-7.x-2.0-beta2.tar.gz"

; Libraries
libraries[fullcalendar][download][type] = "get"
libraries[fullcalendar][download][url] = "http://arshaw.com/fullcalendar/downloads/fullcalendar-1.6.4.zip"

